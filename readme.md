### QUESTION 1
* run the application 
* on postman enter the url : http://localhost:8080/v1/api/openHours
* POST Method , with sample json attached in "resources/static/sample.json"
* output will be  printed to the console
* a sample test method is  bundled with the application 



### QUESTION 2
* As regards the data structure , I think it is okay as it represent a One to Many relationship between a "DAY" and the "OPENING / CLOSING PERIODS"
* However I would prefer that "CLOSING PERIODS" do not overlap if it goes to the next day . Each "CLOSING PERIOD" should terminate by 12 midgnight
eg
* if the restaurant should open on "WEDNESDAY" by "6PM" and close on "THURSDAY" by "1AM" then it should be 
* WEDNESDAY - "6PM" to "12AM"
* THURSDAY - "12AM" to "1AM"
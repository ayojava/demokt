package org.js.demokt

enum class DaysEnum(val day: String) {
    MONDAY("monday"),
    TUESDAY("tuesday") ,
    WEDNESDAY("wednesday") ,
    THURSDAY ("thursday"),
    FRIDAY ("friday"),
    SATURDAY ("saturday"),
    SUNDAY("sunday")
}
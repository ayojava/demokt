package org.js.demokt

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

const val API = "/v1/api"
const val OPEN_API = "/openHours"

@RestController
@RequestMapping(API)
class DemoController (val restaurantService: RestaurantService){

    @PostMapping(OPEN_API)
    fun handleOpenHours(@RequestBody inputDataList: List<InputData>) : ResponseEntity<Void>{
        restaurantService.handlePayload(inputDataList)
        return ResponseEntity<Void>(HttpStatus.OK)
    }

}
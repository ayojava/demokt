package org.js.demokt

enum class TypeEnum(val type: String) {
    OPEN("open"),CLOSE("close")
}
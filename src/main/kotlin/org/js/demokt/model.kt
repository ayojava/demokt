package org.js.demokt

import org.springframework.stereotype.Service


data class InputData(val dayOfWeek : DaysEnum , val openingHours: List<OpeningHours>)

data class OpeningHours(val type : TypeEnum , val value : Long)

data class NodeData(val dayOfWeek : DaysEnum,val type : TypeEnum?=null , val value : Long)

class Node<NodeData>(var value: NodeData ,var next: Node<NodeData>? = null,var previous:Node<NodeData>? = null)

@Service
class RestaurantService(){

    fun handlePayload(inputDataList : List<InputData>){
        val linkedNode = LinkedNode<NodeData>()
        inputDataList.stream()
            .forEach { it ->
                    val dayOfWeek = it.dayOfWeek
                    if(it.openingHours.isEmpty()){
                        val nodeData = NodeData(dayOfWeek, null, 0)
                        linkedNode.appendToTail(nodeData)
                    }else{
                        it.openingHours.forEach {
                            val nodeData = NodeData(dayOfWeek, it.type, it.value)
                            linkedNode.appendToTail(nodeData)
                        }
                    }
                }
        linkedNode.traverseList()
    }
}



class LinkedNode<T> {

    private var head: Node<NodeData>? = null
    private var tail: Node<NodeData>? = null
    private var size = 0

    private var outputMap  = LinkedHashMap<DaysEnum,String>()

    private fun isEmpty(): Boolean {
        return size == 0
    }

    fun appendToTail(aNode : NodeData){
        var newNode = Node(aNode)
        if(isEmpty()){
            head = newNode
        }else{
            if(head?.next == null){
                head?.next = newNode
                newNode.previous = head
            }else{
                tail?.next = newNode
                newNode?.previous = tail
            }
            tail = newNode
        }
        size++
    }

    fun traverseList(){
        println("Size of LinkedList is  $size")
        var aNode = head
        while(aNode != null){
            val startNodeData = aNode.value
            val nextNodeData = aNode.next?.value
            if (startNodeData.type == null){
                outputMap[startNodeData.dayOfWeek] = TypeEnum.CLOSE.name
                aNode = aNode.next
            }else{
                handleNodeData(startNodeData, nextNodeData!!)
                aNode = aNode.next?.next
            }
        }
        for ((key, value) in outputMap) {
            println("$key = $value")
        }
    }

    fun handleNodeData(startNodeData: NodeData , nextNodeData: NodeData){

        val headNodeDataValue: Long = startNodeData.value

        val nextNodeDataValue = nextNodeData?.value
        var outputStr = convertToTime(headNodeDataValue) + " - " + convertToTime(nextNodeDataValue)

        if(outputMap.containsKey(startNodeData.dayOfWeek)){
            val strValue = outputMap.getValue(startNodeData.dayOfWeek) + ", " +  outputStr
            outputMap.replace(startNodeData.dayOfWeek,strValue)
        }else{
            outputMap[startNodeData.dayOfWeek] = outputStr
        }
    }

    private fun convertToTime(unixTime : Long): String{
        val second = unixTime % 60
        val hour = unixTime / 3600
        val minutes = (unixTime / 60) % 60

        val secondFormat: String = if(second < 10) "0$second" else "$second"
        val minuteFormat : String = if(minutes < 10) "0$minutes" else "$minutes"
        val presentTime = ("$hour"+"$minuteFormat"+"$secondFormat").toLong()
        return when{
            presentTime < 120000 -> "$hour.$minuteFormat:$secondFormat AM"
            (presentTime >= 120000) && (presentTime <= 125959)  -> "$hour.$minuteFormat:$secondFormat PM"
            presentTime > 125959 -> (hour-12).toString()+".$minuteFormat:$secondFormat PM"
            else -> "Invalid Time"
        }
    }

}
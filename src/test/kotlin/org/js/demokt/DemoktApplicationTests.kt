package org.js.demokt

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.http.*


@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class DemoktApplicationTests (@Autowired private val restTemplate: TestRestTemplate){

    @Test
    fun testEndpoint() {
        val openHours1 = OpeningHours(TypeEnum.OPEN, 32400)
        val closeHours1 = OpeningHours(TypeEnum.CLOSE, 37800)
        val samplePeriod1 = listOf(openHours1,closeHours1)

        val openHours2 = OpeningHours(TypeEnum.OPEN, 45400)
        val closeHours2 = OpeningHours(TypeEnum.CLOSE, 86300)
        val samplePeriod2 = listOf(openHours2,closeHours2)

        val openHours3 = OpeningHours(TypeEnum.OPEN, 64800)
        val closeHours3 = OpeningHours(TypeEnum.CLOSE, 3600)

        val openHours4 = OpeningHours(TypeEnum.OPEN, 32400)
        val closeHours4 = OpeningHours(TypeEnum.CLOSE, 39600)

        val openHours5 = OpeningHours(TypeEnum.OPEN, 57600)
        val closeHours5 = OpeningHours(TypeEnum.CLOSE, 82800)

        val monday1 = InputData(DaysEnum.MONDAY , samplePeriod1)
        val monday2 = InputData(DaysEnum.MONDAY , samplePeriod2)
        val tuesday = InputData(DaysEnum.TUESDAY , listOf())
        val wednesday = InputData(DaysEnum.WEDNESDAY , listOf(openHours1))
        val thursday1 = InputData(DaysEnum.THURSDAY , listOf(closeHours1))
        val thursday2 = InputData(DaysEnum.THURSDAY , samplePeriod1)
        val thursday3 = InputData(DaysEnum.THURSDAY , samplePeriod2)

        val friday1 = InputData(DaysEnum.FRIDAY , listOf(openHours3))
        val saturday1 = InputData(DaysEnum.SATURDAY , listOf(closeHours3))

        val saturday2 = InputData(DaysEnum.SATURDAY , listOf(openHours4,closeHours4))
        val saturday3 = InputData(DaysEnum.SATURDAY , listOf(openHours5,closeHours5))

        val mapper = jacksonObjectMapper()
        val listOf = listOf(monday1, monday2, tuesday, wednesday, thursday1, thursday2, thursday3, friday1, saturday1, saturday2, saturday3)

        val httpHeaders = HttpHeaders();
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        val httpEntity = HttpEntity<String>(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listOf), httpHeaders)
        val postForObject: ResponseEntity<String> = restTemplate.postForEntity(API + OPEN_API, httpEntity, String::class.java)
        assertEquals(postForObject?.statusCode, HttpStatus.OK)
    }

}
